FROM bash
WORKDIR /script
COPY ./vault-secret-to-env.sh .

RUN apk add jq
RUN apk add curl

CMD ["bash", "vault-secret-to-env.sh"]
