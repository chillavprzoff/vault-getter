#!/bin/bash

if [[ $VAULTG_ADDR == "" ]]; then
  echo "ERROR: Please set VAULTG_ADDR variable to vault server address!!!"
  exit 1
fi

if [[ $VAULTG_USERNAME == "" ]]; then
  echo "ERROR: Please set VAULTG_USERNAME variable to vault server username!!!"
  exit 1
fi

if [[ $VAULTG_PASSWORD == "" ]]; then
  echo "ERROR: Please set VAULTG_PASSWORD variable to vault server password!!!"
  exit 1
fi

if [[ $VAULTG_SECRET_PATH == "" ]]; then
  echo "ERROR: Please set VAULTG_SECRET_PATH variable to path to the secret!!!"
  exit 1
fi

if [[ $VAULTG_SECRET_ENGINE_NAME == "" ]]; then
  VAULTG_SECRET_ENGINE_NAME="secret"
fi

if [[ $VAULTG_SECRET_DIRECTORY == "" ]]; then
  VAULTG_SECRET_DIRECTORY="/secret"
fi

if [[ $VAULTG_ENV_FILE_NAME == "" ]]; then
  VAULTG_ENV_FILE_NAME=".env"
fi

if [[ $VAULTG_SKIP_SSL_VERIFY == "" ]]; then
  VAULTG_SKIP_SSL_VERIFY=false
fi

VAULTG_TOKEN=$(curl $(if [[ $VAULTG_SKIP_SSL_VERIFY == true ]]; then echo "--insecure"; fi) --request POST --data '{ "password": "'$VAULTG_PASSWORD'" }' $VAULTG_ADDR/v1/auth/userpass/login/$VAULTG_USERNAME | jq ".auth.client_token" | sed "s/\"//g")

if [[ $VAULTG_TOKEN == null ]] || [[ $VAULTG_TOKEN == "null" ]] || [[ $VAULTG_TOKEN == "" ]]; then
  echo
  echo "ERROR: cannot login to vault server!!!"
  exit 1
fi

if [[ $VAULTG_DONT_REMOVE_ENV != true ]]; then
  if [[ -f $VAULTG_SECRET_DIRECTORY/$VAULTG_ENV_FILE_NAME ]]; then
    if ! rm -rf $VAULTG_SECRET_DIRECTORY/$VAULTG_ENV_FILE_NAME; then
      cat /dev/null >$VAULTG_SECRET_DIRECTORY/$VAULTG_ENV_FILE_NAME
    fi
  fi
fi

values=$(curl $(if [[ $VAULTG_SKIP_SSL_VERIFY == true ]]; then echo "--insecure"; fi) --header "X-Vault-Token: $VAULTG_TOKEN" --request GET $VAULTG_ADDR/v1/$VAULTG_SECRET_ENGINE_NAME/data/$VAULTG_SECRET_PATH | jq ".data.data")
for s in $(echo $values | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]"); do
  if ! echo $s | grep -q "="; then
    secret="$previousSecret $s"
    sed -i -e "/$previousSecret/,+1d" $VAULTG_SECRET_DIRECTORY/$VAULTG_ENV_FILE_NAME
  else
    secret=$s
  fi
  echo $secret >>raw.env
  previousSecret=$secret
done

### Cleaning duplicate keys ##

keys=($(awk -F= '!seen[$1]++ {gsub(/[" ]/, "", $1); print $1}' raw.env))

pattern=$(
  IFS="|"
  echo "${keys[*]}"
)

# Process the file with awk
awk -v pattern="$pattern" '
  BEGIN {
    # Create an array for unique keys
    n = split(pattern, keyArray, "|")
    for (i = 1; i <= n; i++) {
      last[keyArray[i]] = ""
    }
  }
  {
    # Extract the key (part before =)
    key = $1
    sub(/=.*/, "", key)

    # If the key is in our list, store its last occurrence
    if (key in last) {
      last[key] = $0
      next
    }
    
    # Otherwise, print the line immediately
    print
  }
  END {
    # Print the last occurrences at the end of the file
    for (i = 1; i <= n; i++) {
      if (last[keyArray[i]] != "") {
        print last[keyArray[i]]
      }
    }
  }
' raw.env >$VAULTG_SECRET_DIRECTORY/$VAULTG_ENV_FILE_NAME
rm -rf raw.env

echo
echo "INFO: success get vault secret!!!"
