package main

import (
  "fmt"
  "log"
  "os"
  "github.com/gofiber/fiber/v2"
  "github.com/joho/godotenv"
)

// use godot package to load/read the .env file and
// return the value of the key
func goDotEnvVariable(key string) string {

  // load .env file
  err := godotenv.Load(".env")

  if err != nil {
    log.Fatalf("Error loading .env file")
  }

  return os.Getenv(key)
}

func main() {
    app := fiber.New()

    app.Get("/", func(c *fiber.Ctx) error {
        return c.SendString("API Healthy and Running..")
    })

    app.Get("/get/:env", func(c *fiber.Ctx) error {
      if goDotEnvVariable(c.Params("env")) != "" {
        str := fmt.Sprintf("%s: %s", c.Params("env"), goDotEnvVariable(c.Params("env")))
        return c.SendString(str)
      } else {
        return c.SendString("Cannot find variable..")
      }
    })

    if goDotEnvVariable("APP_PORT") != "" {
      appPort := fmt.Sprintf(":%s", goDotEnvVariable("APP_PORT"))
      app.Listen(appPort)
    } else {
      app.Listen(":3000")
    }

}
