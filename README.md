# Vault Getter

Use for get vault values to particular output

## GLOBAL ENVIRONMENT VARIABLE

| Environment Name | Default Value | Type | Description | Mandatory |
|---|---|---|---|---|
| VAULTG_ADDR  | null | String | Vault address/url | true |
| VAULTG_USERNAME  | null | String | Vault username | true |
| VAULTG_PASSWORD  | null | String | Vault user password | true |
| VAULTG_SECRET_ENGINE_NAME | secret | String | Name of vault secret engine, default to 'secret' | |
| VAULTG_SECRET_PATH | null | String | Vault key value secret path | true |
| VAULTG_SECRET_DIRECTORY | /secret | String | Secret/Output directory | |
| VAULTG_ENV_FILE_NAME  | .env | String | Output Environment Variable file name | |
| VAULTG_DONT_REMOVE_ENV | null / false | Boolean | By default if **VAULTG_ENV_FILE_NAME** file exists it will remove the file first before getting the Environment Variable, so if it set to `true`, the file will not be deleted | |
| VAULTG_SKIP_SSL_VERIFY | false | Boolean | If true skip ssl verificatoin of the target vault server | |

## Examples

### Docker Compose

```yaml
services:
  app:
    container_name: vaultg-exampple-app
    image: registry.gitlab.com/chillavprzoff/vault-getter/example-app
    volumes:
      - ./.env:/usr/src/app/.env
    ports:
      - "3000:3000"
    depends_on:
      init-app:
        condition: service_completed_successfully

  init-app:
    container_name: vaultg-init-app
    image: registry.gitlab.com/chillavprzoff/vault-getter
    environment:
      - VAULTG_ADDR=https://vault.example.com
      - VAULTG_USERNAME=username
      - VAULTG_PASSWORD=password
      - VAULTG_SECRET_PATH=path/to/secret
    volumes:
      - ./.env:/secret/.env
```
